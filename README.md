# Accounts Manager
A website to detail the contents of your back accounts.

## License
This software is under the GNU Affero General Public License (AGPL).
Please refer to the LICENSE file for details

## Installation
To run Accounts Manager on your machine, get the code and open the directory.
```bash
git clone https://gitlab.com/finxol/accounts-manager.git && cd accounts-manager
```

All initialisation steps are automated in the `init.sh` script.
To setup everything, simply run
```bash
bash init.sh
```

Now you should be good to go.
Start the website by running :
```bash
npm run start
```
You should now be able to access Accounts Manager from [localhost:35690](http://127.0.0.1:35690).
Check the port printed in the terminal to make sure you are using the right one.
