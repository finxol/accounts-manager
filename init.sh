#! /bin/sh

# install all dependencies
npm i

# setup the database
cat src/db/init.sql | sqlite3 src/db/account-management.db

# set id
id=$(uuid)
# set username
read -rp "Username: " user
# set password
read -rsp "Password: " pass
hash=$(echo -n $var | sha256sum)
hash=$(echo -n "${hash:0:64}$user" | sha256sum)
pass=${hash:0:64}

sqlite3 src/db/account-management.db "INSERT INTO Profiles (id, username, password) VALUES ('$id', '$user', '$pass')"
