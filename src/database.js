const sqlite3 = require('sqlite3').verbose();


/**
 * Check the password against the saved one
 * @param {string} username Username for which to check the password
 * @param {string} password Password to check
 * @returns {Promise<string|null>} id of the account if passwords match, null otherwise
 */
async function check_password(username, password) {
    // open database in disk file
    let db = new sqlite3.Database('./src/db/account-management.db', (err) => {
        if (err) throw err;
    });

    console.log(password)

    let query = `SELECT id FROM Profiles WHERE
            Profiles.username = ? AND
            Profiles.password = ?
        ;`;

    let results = [];
    await db.all(query, [username, password], (err, rows) => {
        if (err) throw err;
        results = rows;
    });


    // close the database connection
     await db.close((err) => {
        if (err) throw err;
    });

    await new Promise(r => setTimeout(r, 20)); // sleep for 20ms
    console.log(results);
    try {
        return results[0].id;
    } catch {
        return null;
    }
}



/**
 * Check the token against the saved one
 * @param {string} profileId Id of the profile for which to check the token
 * @param {string} token Token to check
 * @returns {Promise<boolean>} true if passwords match and the delay hasn't been exceeded, false otherwise
 */
async function check_token(profileId, token) {
    // open database in disk file
    let db = new sqlite3.Database('./src/db/account-management.db', (err) => {
        if (err) throw err;
    });

    let query = `SELECT id FROM Profiles
                 WHERE Profiles.id = ?
                    AND ( strftime('%s','now') - Profiles.last_logged <= 86400 /* =1 day */
                    AND Profiles.token = ? );`;

    let results = [];
    await db.each(query, [profileId, token], (err, row) => {
        if (err) throw err;
        results.push(row);
    });

    // close the database connection
    db.close((err) => {
        if (err) throw err;
    });

    await new Promise(r => setTimeout(r, 20)); // sleep for 20ms
    return results.length === 1;
}



/**
 * Save the token to the database
 * @param {string} profileId Id of the profile for which to save the token
 * @param {string} password Password for the profile
 * @param {string} token Token to save
 * @returns {Promise<boolean>} true if password matches and token saved, false otherwise
 */
async function save_token(profileId, password, token) {
    // open database in disk file
    let db = new sqlite3.Database('./src/db/account-management.db', (err) => {
        if (err) throw err;
    });

    let query = `UPDATE Profiles
                SET token = ?, last_logged = strftime('%s','now')
                WHERE id = ? AND password = ?;`;

    let result = false;
    await db.run(query, [token, profileId, password], err => {
        if (err) {
            throw err;
        } else {
            console.log('token saved ok');
            result = true;
        }
    });

    // close the database connection
    db.close((err) => {
        if (err) throw err;
    });

    await new Promise(r => setTimeout(r, 20)); // sleep for 20ms
    return result;
}


/**
 * Read the latest balance for an account from the database
 * @param {string} profileId Username of the profile to which the account belongs
 * @param {string} account Account for which to check the balance
 * @returns {Promise<Array>} Array of objects containing the detail of the account
 */
async function latest(profileId , account) {
    // open database in disk file
    let db = new sqlite3.Database('./src/db/account-management.db', (err) => {
        if (err) throw err;
    });

    let query = `SELECT Det.id, Det.title, Det.value, Det.account FROM Profiles AS Pr
                    JOIN Accounts AS Acc on Pr.id = Acc.profileId
                    JOIN Details AS Det on Acc.id = Det.account
                 WHERE Acc.id = ? AND Pr.id = ?;`;

    let results = [];
    await db.each(query, [account, profileId], (err, row) => {
        if (err) throw err;
        results.push(row);
    });

    // close the database connection
    db.close((err) => {
        if (err) throw err;
    });

    await new Promise(r => setTimeout(r, 20)); // sleep for 20ms
    return results;
}



async function save(data) {
    let ok = true;

    // open database in disk file
    let db = new sqlite3.Database('./src/db/account-management.db', (err) => {
        if (err) throw err;
    });

    for (const row of data) {
        let query = `INSERT OR REPLACE INTO Details (id, title, value, account)
                    VALUES(?, ?, ?, ?)`;

        let result = false;
        await db.run(query, [row.id, row.title, row.value, row.account], err => {
            if (err) {
                ok = false;
                throw err;
            } else {
                console.log('row saved ok');
                result = true;
            }
        });
    }


    // close the database connection
    db.close((err) => {
        if (err) throw err;
    });

    await new Promise(r => setTimeout(r, 20)); // sleep for 20ms
    return ok;
}


/**
 * Get all the accounts belonging to a user
 * @param {string} profileId Username of the profile for which to load the accounts list
 * @returns {Promise<Array>} Array of objects containing the account titles and ids
 */
async function accounts(profileId) {
    // open database in disk file
    let db = new sqlite3.Database('./src/db/account-management.db', (err) => {
        if (err) throw err;
    });

    let query = `SELECT Acc.id, Acc.title FROM Profiles AS Pr
                    JOIN Accounts AS Acc on Pr.id = Acc.profileId
                 WHERE Pr.id = ?;`;

    let results = [];
    await db.each(query, [profileId], (err, row) => {
        if (err) throw err;
        results.push(row);
    });

    // close the database connection
    db.close((err) => {
        if (err) throw err;
    });

    await new Promise(r => setTimeout(r, 20)); // sleep for 20ms
    return results;
}



module.exports = { check_token, check_password, save_token, latest, save, accounts };
