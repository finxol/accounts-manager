let express = require('express');
let router = express.Router();
const fs = require("fs");
let bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');

const crypto = require('crypto');
const jwt = require('jsonwebtoken');
let db = require('./database');

router.use(bodyParser.urlencoded({ extended: true }));
router.use(bodyParser.json());
router.use(cookieParser());


/**
 * SHA256 hash an object
 * @param str Object to hash
 * @returns {string} SHA256 of the object
 */
function hash(str) {
    return crypto.createHash('sha256')
        .update(str)
        .digest('hex');
}

/**
 * Authenticate a user then execute actions
 * @param {Request<>} req Request object
 * @param {Response<>} res Response object
 * @param next Next function to execute
 * @returns {Promise<void>}
 */
async function requireLogin(req, res, next) {
    if (req.cookies['token']) {
        let publicKey = fs.readFileSync(__dirname + '/keys/public.key');
        let decodedJwt = jwt.verify(req.cookies.token, publicKey);
        let authorised = await db.check_token(decodedJwt.id, decodedJwt.token);

        console.log({authorised});
        if (authorised) {
            req.id = decodedJwt.id;
            next();
        } else {
            res.status(403).redirect('/login');
        }
    } else {
        console.log(req.path)
        if (req.path === '/auth') {
            next();
        } else {
            res.status(401).redirect('/login');
        }
    }
}

// Automatically apply the `requireLogin` middleware to all
// routes starting with `/api/v1`
router.all("*", requireLogin, function(req, res, next) {
    next(); // if the middleware allowed us to get here, just move on to the next route handler
});


/**
 * GET account keys
 * Format : /api/v1/accounts
 */
router.get("/accounts", async (req, res) => {
    let accounts = await db.accounts(req.id);
    console.log({accounts});
    res.send(accounts);
});

/**
 * GET last balance
 * Format : /api/v1/latest/[name of the account]
 */
router.get("/latest/:account", async (req, res) => {
    let latest = await db.latest(req.id, req.params['account']);
    res.send(latest);
});


/**
 * PUT save changes
 * Format : /api/v1/save/[name of the account]
 */
router.put('/save/:account', async (req, res) => {
    try {
        await db.save(req.body);
        res.sendStatus(200);
    } catch (e) {
        console.error(e);
        res.sendStatus(500)
    }
});


/**
 * POST authenticate user
 * Format : /api/v1/auth
 */
router.post('/auth', async (req, res) => {
    let passwordHash = hash(req.body.password + req.body.username);
    let accountId = await db.check_password(req.body.username, passwordHash);

    if (accountId) {
        let privateKey = fs.readFileSync(__dirname + '/keys/private.key');
        let data = {
            id: accountId,
            token: hash(Math.random().toString().slice(2))
        };

        try {
            const expiry = 900; // 15 mins in seconds
            const d = new Date(req.body.timestamp + expiry*1000); // In milliseconds

            let token = jwt.sign(data, privateKey, {
                algorithm: 'RS256',
                expiresIn: expiry
            });
            await db.save_token(data.id, passwordHash, data.token);

            res.cookie('token', token, {expires: d, hostOnly: false, httpOnly: true, Secure: true, path: '/', sameSite: 'strict'});
            res.sendStatus(200);
        } catch (e) {
            console.error(e);
            res.sendStatus(500);
        }

    } else {
        res.sendStatus(403);
    }
});



module.exports = router;
