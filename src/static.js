let express = require('express');
let router = express.Router();
let path = require('path');
let fs = require('fs');
let exec = require('child_process').exec;

const jwt = require('jsonwebtoken');
let db = require('./database');

const cookieParser = require('cookie-parser');
router.use(cookieParser());



/**
 * GET favicon
 * Format : /favicon.ico
 */
router.get("/favicon.ico", (req, res) => {
    res.sendFile(path.join(__dirname, "..", "public", "assets", "favicons", "favicon.ico"));
});


/**
 * GET gitlab webhook for auto deploy
 * Format : /gitlab
 */
router.post('/gitlab', (req, res) => {
    exec("git pull", function(err, stdout, stderr) {
        if (err) throw err;
        if (stderr) {
            console.error(stderr);
            res.status(500).send(stderr);
        }
        let output = stdout;
        exec("cd /server/accounts-manager/ && forever restart server.js", function(err, stdout, stderr) {
            if (err) throw err;
            if (stderr) {
                console.error(stderr);
                res.status(500).send(stderr);
            }
            output += stdout;
            console.log(output);
            res.send(output);
        });
    });
});


/**
 * GET login
 * Format : /login
 */
router.get('/login', (req, res) => {
    res.sendFile(path.join(__dirname, "..", "public",  "login.html"));
});


/**
 * GET static files
 * Format : /[any file]
 */
router.get("*", async (req, res) => {
    /**
     * GET static files (except html)
     * Format : [any uri].[any file extension]
     */
    if (req.url.includes('.')) {
        let file = path.join(__dirname, "..", "public", req.url);
        if (fs.existsSync(file)) {
            res.sendFile(file);
        } else {
            res.status(404).sendFile(path.join(__dirname, "..", "public",  "404.html"));
        }
    }

    /**
     * GET html pages
     * Format : /[any uri without file extension]
     */
    else {
        if (req.cookies['token'] !== undefined) {
            let publicKey = fs.readFileSync(__dirname + '/keys/public.key');
            let decodedJwt = jwt.verify(req.cookies.token, publicKey);
            let authorised = await db.check_token(decodedJwt.id, decodedJwt.token);

            console.log({authorised});
            if (authorised) {
                if (req.url === "/") {
                    res.sendFile(path.join(__dirname, "..", "public",  "index.html"));
                } else {
                    let file = path.join(__dirname, "..", "public", req.url.split('?')[0] + ".html");
                    if (fs.existsSync(file)) {
                        res.sendFile(file);
                    } else {
                        res.status(404).sendFile(path.join(__dirname, "..", "public",  "404.html"));
                    }
                }
            } else {
                res.status(403).redirect('/login');
            }
        } else {
            console.log('no cookie set');
            res.status(401).redirect('/login');
        }
    }
});



module.exports = router;