DROP TABLE IF EXISTS Profiles;
CREATE TABLE Profiles (
    id CHAR(36) NOT NULL PRIMARY KEY,
    username TEXT NOT NULL,
    password TEXT NOT NULL,
    token TEXT,
    last_logged INTEGER
);

DROP TABLE IF EXISTS Accounts;
CREATE TABLE Accounts (
    id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    title TINYTEXT NOT NULL,
    profileId CHAR(36) NOT NULL,
    FOREIGN KEY(profileId) REFERENCES Profiles(id)
);

DROP TABLE IF EXISTS Details;
CREATE TABLE Details (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    title TINYTEXT NOT NULL,
    value FLOAT NOT NULL,
    account INTEGER NOT NULL,
    FOREIGN  KEY(account) REFERENCES Accounts(id)
);