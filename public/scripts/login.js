/**
 * Create a dialog box in the web page to inform the user of activity
 * @param {string} type Type of message (error, success, warning)
 * @param {string} message Message to display
 */
function create_alert(type, message) {
    let alertBanner = document.createElement('div');
    alertBanner.classList.add("alert", type);
    alertBanner.textContent = message;
    document.body.appendChild(alertBanner);
    let clear = setTimeout(() => {
        document.body.removeChild(alertBanner);
    }, 4000);
    alertBanner.onclick = () => {
        document.body.removeChild(alertBanner);
        clearTimeout(clear);
    };
}


let form = document.getElementById('login');
form.onsubmit = e => {
    e.preventDefault();
    let data = {
        username: document.getElementById('username').value,
        password: SHA256(document.getElementById('password').value),
        timestamp: Date.now()
    };

    let req = new XMLHttpRequest();
    req.open('POST', '/api/v1/auth', true);
    req.onreadystatechange = () => {
        if (req.readyState === XMLHttpRequest.DONE) {
            if (req.status === 200) {
                create_alert("success", "Logged in successfully");
                setTimeout(() => {
                    location.pathname = "/";
                }, 500);
            } else if (req.status === 403) {
                create_alert("error", "Incorrect credentials. Please try again.");
            } else if (/50*/.test(req.status.toString())) {
                create_alert("error", "Internal server error. Please try again later.");
            }
        }
    };

    req.setRequestHeader('Content-Type', 'application/json');
    req.setRequestHeader('X-Requested-With', 'XMLHttpRequest');

    req.send(JSON.stringify(data));
}


let passwordField = document.getElementById('password');
document.getElementById('showPassword').onclick = () => {
    if (passwordField.type === "password") {
        passwordField.type = "text"
    } else if (passwordField.type === "text") {
        passwordField.type = "password";
    }
}