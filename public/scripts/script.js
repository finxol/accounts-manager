/**
 * Create a cookie in the client's browser
 * @param {string} cname Name to gice to the cookie
 * @param {string} cvalue Value to give to the cookie
 * @param {int} exdays Number of days until expiry
 */
function setCookie(cname, cvalue, exdays) {
    const d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    let expires = "expires="+ d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

/**
 * Create a dialog box in the web page to inform the user of activity
 * @param {string} type Type of message (error, success, warning)
 * @param {string} message Message to display
 */
function create_alert(type, message) {
    let alertBanner = document.createElement('div');
    alertBanner.classList.add("alert", type);
    alertBanner.textContent = message;
    document.body.appendChild(alertBanner);
    let clear = setTimeout(() => {
        document.body.removeChild(alertBanner);
    }, 4000);
    alertBanner.onclick = () => {
        document.body.removeChild(alertBanner);
        clearTimeout(clear);
    };
}


/**
 * Create a section containing the balances and details of an account
 * @param {string} acName Name of the account
 */
class Section {
    constructor(acc) {
        this.accountName = acc.title;
        this.accountId = acc.id;
        this.latest_balance;
    }

    /**
     * Get the latest balance from server for the account
     */
    get latest_balance() {
        let req = new XMLHttpRequest();
        req.onreadystatechange = () => {
            if (req.readyState === XMLHttpRequest.DONE) {
                if (req.status === 200) {
                    this.balance = JSON.parse(req.responseText);
                    this.create_section();
                } else if (req.status === 404) {
                    create_alert('error', "No balance found")
                }
            }
        };

        let url = `/api/v1/latest/${this.accountId}`;
        req.open('GET', url, true);
        req.send();
    }

    /**
     * Create the DOM element for the section
     */
    create_section() {
        try {
            this.section.parentElement.removeChild(this.section);
        } catch {
            console.log("section not yet initialised");
        }

        this.section = document.createElement('section');

        this.total = 0;
        this.balance.forEach((elem) => {
            this.total += parseFloat(elem.value);
        });
        let span = document.createElement('span');
        span.innerText = this.total + '€';

        let p = document.createElement('p');
        p.innerText = this.accountName + ': ';
        p.appendChild(span);

        let img = document.createElement('img');
        img.src = '/assets/pen.webp';
        img.alt = 'Edit';

        let button = document.createElement('button');
        button.appendChild(img);
        button.onclick = () => {
            this.edit();
        };
        button.title = 'Edit';

        let div = document.createElement('div');
        div.appendChild(p);
        div.appendChild(button);

        let ul = document.createElement('ul');
        this.balance.forEach(elem => {
            let li = document.createElement('li');
            li.innerHTML = `${elem.title} &rarr; ${elem.value}€`;
            li.setAttribute('data-id', String(elem.id));
            ul.appendChild(li);
        });

        this.section.appendChild(div);
        this.section.appendChild(ul);

        document.getElementsByTagName('article')[0].appendChild(this.section);
    }

    /**
     * Modifies the content of the section to allow modification of the details
     */
    edit() {
        let lines = [];
        let areas = this.section.querySelectorAll('ul li');
        areas.forEach((line, i) => {
            lines.push({});
            [ lines[i].title, lines[i].value ] = line.innerText.split(' → ');
            console.log(lines);

            let inputTitle = document.createElement('input');
            inputTitle.type = 'text';
            inputTitle.value = lines[i].title;
            inputTitle.setAttribute('data-title', '');

            let rarr = document.createElement('span');
            rarr.innerHTML = '&rarr;';

            let inputValue = document.createElement('input');
            inputValue.type = 'number';
            inputValue.value = lines[i].value.split('€')[0];
            inputValue.step = '0.01';
            inputValue.setAttribute('data-balance', '');

            line.innerHTML = '';
            line.appendChild(inputTitle);
            line.appendChild(rarr);
            line.appendChild(inputValue);
            lines[i].node = line;
        });

        let submitButton = document.createElement('input');
        submitButton.type = 'submit';
        submitButton.style.display = 'none';

        let form = document.createElement('form');
        form.onsubmit = (e) => {
            e.preventDefault();
            this.save();
        };

        let ul = this.section.querySelector('ul');
        this.section.removeChild(ul);
        this.section.appendChild(form);
        lines.forEach(line => {
            ul.appendChild(line.node);
        });
        form.appendChild(ul);
        form.appendChild(submitButton);

        let buttonImg = this.section.querySelector('div button img');
        buttonImg.src = '/assets/save.png';
        buttonImg.alt = 'Save';
        buttonImg.parentElement.onclick = () => {
            submitButton.click();
        };
    }

    /**
     * Save the modification to the server
     */
    save() {
        let req = new XMLHttpRequest();
        req.open('PUT', `/api/v1/save/${this.accountName}`, true);
        req.setRequestHeader("Content-Type", "application/json");
        req.onreadystatechange = () => {
            if (req.readyState === XMLHttpRequest.DONE) {
                if (req.status === 200) {
                    create_alert("success", "New balance saved successfully");
                    this.latest_balance;
                } else if (/(50*)|404/.test(req.status.toString())) {
                    create_alert("error", "An error occurred while saving your changes please try again later");
                    this.latest_balance;
                }
            }
        };

        let inputLines = this.section.querySelectorAll('form ul li');
        inputLines.forEach((line, i) => {
            this.balance[i].value = line.querySelector('input[data-balance]').value;
            this.balance[i].title = line.querySelector('input[data-title]').value;
        });

        req.send(JSON.stringify(this.balance));
    }
}




let accountFields = [];
/**
 *
 */
(() => {
    let getAccounts = new XMLHttpRequest();
    getAccounts.onreadystatechange = () => {
        if (getAccounts.readyState === XMLHttpRequest.DONE) {
            if (getAccounts.status === 200) {
                let accounts = JSON.parse(getAccounts.responseText);
                accounts.forEach(acc => {
                    accountFields.push(new Section(acc));
                });
            } else if (getAccounts.status === 404) {
                create_alert('error', "No accounts found");
            }
        }
    };

    getAccounts.open('GET', '/api/v1/accounts', true);
    getAccounts.send();
})()
