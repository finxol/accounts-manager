#! /usr/bin/env node

const express = require('express');
const app = express();
const staticFiles = require('./src/static.js');
const api = require('./src/api.js');
// const dynamicPages = require('./src/dynamic.js');

app.use('/api/v1/', api);
// app.use('/', dynamicPages);
app.use('/', staticFiles);

app.disable('x-powered-by');


let port = process.env.PORT || 35690;
app.listen(port, (err) => {
    if (err) throw err;
    console.log(`Node server is running on port ${port}...`);
});